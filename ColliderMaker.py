bl_info = {
    "name": "Coollider",
    "author": "mbbmbbmm",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Properties",
    "description": "",
    "warning": "",
    "doc_url": "",
    "category": "",
}

import re
import bpy
from bpy.types import Operator


class AddCollider(bpy.types.Operator):
    bl_idname = "op.add_collider"
    bl_label = "Add Collider"

    def execute(self, context):
        #if len(bpy.context.selected_objects) != 1:
        #    print("please select one parent object!")
        #    #TODO warning in blender
        #else:
        #    parent_obj = bpy.context.view_layer.objects.active
        #    bpy.ops.mesh.primitive_cube_add()
        #    col_obj = bpy.context.view_layer.objects.active
        #    col_obj["Collider"]="BoxCol"
        #    suffixes = []
        #    lowest_free = 0
        #    for ob in bpy.context.visible_objects:
        #        if ('col_box.' in ob.name):
        #            numbers = re.findall(r'\b\d+\b', ob.name)
        #            suffixes.append(int(numbers[0]))
        #            lowest_free = min(set(range(1, len(suffixes) + 1)) - set(suffixes))

        #    col_obj.name = "col_box." + str(lowest_free).zfill(3)
        #    col_obj.display_type = 'WIRE'
        #    col_obj.parent = parent_obj
        #    print("done")
        # TODO don't know if i need this:
        # orig_parent_obj = bpy.context.view_layer.objects.active
        sel_objs = bpy.context.selected_objects
        bpy.ops.mesh.primitive_cube_add()
        #TODO do I need the bpy.context or can use the context from argument?
        col_obj = bpy.context.view_layer.objects.active 
        col_obj["Collider"]="BoxCol"
        col_obj.display_type = 'WIRE'

        for ob in bpy.context.selected_objects:
            ob.select_set(False)

        # optional oder rausfinden, ob datablock der gleiche, sonst nicht gelinkt, oder...
        for ob in sel_objs:
            col_obj.select_set(True)
            bpy.ops.object.duplicate(linked=True)
            col = bpy.context.object
            col.parent = ob
            col.select_set(False)

        #for ob in bpy.context.selected_objects:
        #    ob.select_set(False)
        # TODO delete col_obj

        return {'FINISHED'}

class MirrorCollider(bpy.types.Operator):
    bl_idname = "op.mirror_collider"
    bl_label = "Mirror Collider"

    def execute(self, context):
        if len(bpy.context.selected_objects) != 1:
            print("please select one collider object!")
            #TODO warning in blender
            return {'FINISHED'}
        bpy.ops.object.mode_set(mode='OBJECT')
        obj = bpy.context.view_layer.objects.active
        obj_name = obj.name
        parent = obj.parent
        if parent is None:
            print("please select a collider with a parent!")
            return {'FINISHED'}
        first_suffix = ".R"
        second_suffix = ".L"
        if obj.location[0] > parent.location[0]:
            first_suffix = ".L"
            second_suffix = ".R"

        obj.name = obj_name + first_suffix
        rot_x = obj.rotation_euler[0]
        rot_y = obj.rotation_euler[1]
        rot_z = obj.rotation_euler[2]
        bpy.ops.object.rotation_clear()

        bpy.ops.object.modifier_add(type='MIRROR')
        # brauche ich die naechste zeile?
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.empty_add(type='PLAIN_AXES', align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
        pivot = bpy.context.view_layer.objects.active
        pivot.name = "Mirror Pivot"
        obj.modifiers["Mirror"].mirror_object = pivot
        bpy.context.view_layer.objects.active = obj
        #bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Mirror")
        bpy.ops.object.modifier_apply(modifier="Mirror")
        bpy.context.view_layer.objects.active = pivot
        bpy.ops.object.delete(use_global=True, confirm=False)
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.separate(type='LOOSE')
        bpy.ops.object.mode_set(mode='OBJECT')
        obj_new = bpy.context.view_layer.objects.selected[0]
        obj_new.name = obj_name + second_suffix
        bpy.context.view_layer.objects.active = obj_new
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
        obj.rotation_euler = (rot_x, rot_y, rot_z)
        obj_new.rotation_euler = (rot_x, -rot_y, -rot_z)
        for o in bpy.context.view_layer.objects.selected:
            o.select_set(False)
        bpy.context.view_layer.objects.active = obj
        bpy.context.active_object.select_set(True)
        return {'FINISHED'}

# Registration

def add_collider_button(self, context):
    self.layout.operator(
        AddCollider.bl_idname,
        text="Add Collider",
        icon='CUBE')

def mirror_collider_button(self, context):
    self.layout.operator(
        MirrorCollider.bl_idname,
        text="Mirror Collider",
        icon='PLUGIN')


def register():
    bpy.utils.register_class(AddCollider)
    bpy.utils.register_class(MirrorCollider)
    bpy.types.VIEW3D_MT_mesh_add.append(add_collider_button)
    bpy.types.VIEW3D_MT_mesh_add.append(mirror_collider_button)

def unregister():
    bpy.utils.unregister_class(AddCollider)
    bpy.utils.unregister_class(MirrorCollider)
    bpy.types.VIEW3D_MT_mesh_add.remove(add_collider_button)
    bpy.types.VIEW3D_MT_mesh_add.remove(mirror_collider_button)

#if __name__ == "__main__":
#    register()

