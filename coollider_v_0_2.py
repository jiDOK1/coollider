# Coollider v 0.2 by mbbmbbmm
# TODO go over the script, mostly the operators for consistency in naming etc.
# TODO UI: Add child collider, Convert to Collider
    # TODO Add and Make: bug - it's not totally in the right spot. Maybe origin to geometry at the very
# end?

bl_info = {
    "name": "Coollider",
    "author": "mbbmbbmm",
    "version": (0, 2, 0),
    "blender": (2, 93, 0),
    "location": "View3D > Object Context Menu",
    "description": "",
    "warning": "",
    "doc_url": "",
    "category": "",
}

import bpy
import re
from mathutils import Euler
from math import radians

#-------------Operators----------------

class OBJECT_OT_add_collider(bpy.types.Operator):
    bl_idname = "op.add_collider"
    bl_label = "Add Collider"
    bl_description = "Add Collider as Child"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    col_type : bpy.props.EnumProperty(
        name = "ColliderType",
        description = "Select the type of Collider",
        items=[
            ('Box',"Box Collider","Add Box Collider as Child"),
            ('Sphere',"Sphere Collider","Add Sphere Collider as Child"),
            ('Capsule',"Capsule Collider","Add Capsule Collider as Child"),
            ('Mesh',"Mesh Collider","Add Mesh Collider as Child"),
        ]
    )

    # active_to_selected : bpy.props.BoolProperty(name = "Link from active to selected")

    def execute(self, context):
        # TODO ensure it is also the right type (mesh object)
        # TODO implement multi object capability with loop

        active_obj = context.view_layer.objects.active
        sel_objs = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
        bpy.ops.object.select_all(action='DESELECT')

        cursor_loc = bpy.context.scene.cursor.location.copy()
        bpy.ops.view3d.snap_cursor_to_center()

#        if len(context.selected_objects) != 1:
#            ShowMessageBox(message = "Please select one parent object!")
#            return {'CANCELLED'}

#        parent = context.view_layer.objects.active
        #if self.active_to_selected:
        #    active_obj.select_set(True)
        #    col = ComputeCollider(context, self, active_obj)
        #    loc = col.location
        #    print("col location")
        #    print(loc)
        #    bpy.ops.object.select_all(action='DESELECT')
        #    # duplicate linked to the other selected objects and set to same relative position
        #    while len(sel_objs) >= 1:
        #        parent = sel_objs.pop()
        #        if parent == active_obj:
        #            continue
        #        col.select_set(True)
        #        context.view_layer.objects.active = col
        #        rot = col.rotation_euler
        #        #scale = col.scale
        #        bpy.ops.object.duplicate(linked=True, mode='DUMMY')
        #        dup = context.view_layer.objects.active
        #        bpy.ops.object.select_all(action='DESELECT')
        #        parent.select_set(True)
        #        context.view_layer.objects.active = parent
        #        dup.parent = parent
        #        bpy.ops.object.select_all(action='DESELECT')
        #        dup.select_set(True)
        #        context.view_layer.objects.active = dup
        #        bpy.ops.object.parent_clear(type='CLEAR_INVERSE')
        #        dup.location = col.location

        #        print("dup location")
        #        print(dup.location)
        #else:
        while len(sel_objs) >= 1:
            parent = sel_objs.pop()
#            grandparent = parent.parent
            parent.select_set(True)
            context.view_layer.objects.active = parent
            col = ComputeCollider(context, self, parent)

        bpy.context.scene.cursor.location = cursor_loc
        return {'FINISHED'}

class OBJECT_OT_make_collider(bpy.types.Operator):
    bl_idname = "op.make_collider"
    bl_label = "Make Collider"
    bl_description = "Make Collider from Mesh"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    col_type : bpy.props.EnumProperty(
        name = "ColliderType",
        description = "Select the type of Collider",
        items=[
            ('Box',"Box Collider","Add Box Collider as Child"),
            ('Sphere',"Sphere Collider","Add Sphere Collider as Child"),
            ('Capsule',"Capsule Collider","Add Capsule Collider as Child"),
            ('Mesh',"Mesh Collider","Add Mesh Collider as Child"),
        ]
    )

    def execute(self, context):
        # TODO ensure it is also the right type (mesh object)
        # TODO implement multi object capability with loop

        sel_objs = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
        bpy.ops.object.select_all(action='DESELECT')

        cursor_loc = bpy.context.scene.cursor.location.copy()
        bpy.ops.view3d.snap_cursor_to_center()

#        if len(context.selected_objects) != 1:
#            ShowMessageBox(message = "Please select one parent object!")
#            return {'CANCELLED'}

#        parent = context.view_layer.objects.active
        while len(sel_objs) >= 1:
            parent = sel_objs.pop()
            grandparent = parent.parent 
            parent.select_set(True)
            context.view_layer.objects.active = parent
            col = ComputeCollider(context, self, parent)
            bpy.ops.object.select_all(action='DESELECT')
            parent.select_set(True)
            bpy.ops.object.delete()
            if grandparent != None:
                bpy.ops.object.select_all(action='DESELECT')
                col.select_set(True)
                grandparent.select_set(True)
                bpy.context.view_layer.objects.active = grandparent
                bpy.ops.object.parent_set(type='OBJECT', keep_transform=True)
                bpy.ops.object.select_all(action='DESELECT')

        bpy.context.scene.cursor.location = cursor_loc
        return {'FINISHED'}

class OBJECT_OT_copy_active_to_selected(bpy.types.Operator):
    bl_idname = "op.copy_active_to_selected"
    bl_label = "Copy Collider from Active to Selected"
    bl_description = "Copy Child Collider from Active Object to Selected Objects"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        active_obj = context.view_layer.objects.active
        sel_objs = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
        bpy.ops.object.select_all(action='DESELECT')

        cursor_loc = bpy.context.scene.cursor.location.copy()
        bpy.ops.view3d.snap_cursor_to_center()

        orig_parent = active_obj
        orig_col = active_obj.children[0]

        orig_parent.select_set(True)
        context.view_layer.objects.active = orig_parent


        #Extra rotation ist die col rotation minus die parent rotation
        orig_parent_rot = orig_parent.rotation_euler.copy()
        orig_col_rot = orig_col.rotation_euler.copy()
        extra_rot = Euler((orig_col_rot.x - orig_parent_rot.x, orig_col_rot.y - orig_parent_rot.y, orig_col_rot.z - orig_parent_rot.z), 'XYZ')
        bpy.ops.object.rotation_clear(clear_delta=False)

        select_and_make_active(orig_col, True, context)
        bpy.ops.object.duplicate(linked=False, mode='DUMMY')
        bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')

        temp_col_dup = context.view_layer.objects.active

        # i now have the orig parent unrotated and a unlinked duplicate of the orig col in the right relative position 

        relative_loc = orig_parent.location - temp_col_dup.location
        bpy.ops.object.select_all(action='DESELECT')
#        # duplicate linked to the other selected objects and set to same relative position
        while len(sel_objs) >= 1:
            new_parent = sel_objs.pop()
            if new_parent == orig_parent:
                continue
            select_and_make_active(orig_col, True, context)
#            #scale = orig_col.scale
            bpy.ops.object.duplicate(linked=True, mode='DUMMY')
            dup = context.view_layer.objects.active
            bpy.ops.object.parent_clear(type='CLEAR')
            bpy.ops.object.rotation_clear()

            select_and_make_active(new_parent, True, context)
            new_parent_rot = new_parent.rotation_euler.copy()
            bpy.ops.object.rotation_clear()

            dup.select_set(True)
            dup.location = new_parent.location - relative_loc
            dup.rotation_euler = extra_rot
            bpy.ops.object.parent_set(type='OBJECT', keep_transform=True)

            select_and_make_active(new_parent, True, context)
            new_parent.rotation_euler = new_parent_rot

        select_and_make_active(temp_col_dup, True, context)
        bpy.ops.object.delete()
        orig_parent.rotation_euler = orig_parent_rot
        bpy.context.scene.cursor.location = cursor_loc


        return {'FINISHED'}

            

class OBJECT_OT_mirror_collider(bpy.types.Operator):
    bl_idname = "op.mirror_collider"
    bl_label = "Mirrored from Existing"
    bl_description = "Add Mirrored Collider using Parent as Mirror Object"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        # TODO !!!!!!!!!  mirror is broken... !!!!!!!!!
        if len(context.selected_objects) != 1 or context.active_object.get('CoolliderType') is None:
            ShowMessageBox(message = "Please select one existing collider!")
            return{'CANCELLED'}

        bpy.ops.object.mode_set(mode='OBJECT')

        cursor_loc = bpy.context.scene.cursor.location.copy()
        bpy.ops.view3d.snap_cursor_to_center()

        obj = bpy.context.view_layer.objects.active
        obj_name = obj.name
        #if obj_name.
        parent = obj.parent
        if parent is None:
            ShowMessageBox(message = "Please select a collider with a parent!")
            return {'CANCELLED'}
        first_suffix = ".R"
        second_suffix = ".L"
        if obj.location[0] < parent.location[0]: # das muss besser gehen, vielleicht dot product
            first_suffix = ".L"
            second_suffix = ".R"
        obj.name = obj_name + first_suffix
        #obj.name = "Dörte"
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
        eul = obj.rotation_euler.copy()
        bpy.ops.object.rotation_clear()
        bpy.ops.object.modifier_add(type='MIRROR')
        obj.modifiers["Mirror"].mirror_object = parent
        bpy.context.view_layer.objects.active = obj# brauche ich?
        bpy.ops.object.modifier_apply(modifier="Mirror")
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.separate(type='LOOSE')
        bpy.ops.object.mode_set(mode='OBJECT')
        obj_new = bpy.context.view_layer.objects.selected[1]
        bpy.context.view_layer.objects.active = obj_new
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
        obj_new.name = obj_name + second_suffix
        #obj_new.name = "Heinz"
        print(obj.name)
        print(obj_new.name)
        obj_new.rotation_euler = Euler((eul.x, -eul.y, -eul.z), 'XYZ')
        obj.rotation_euler = eul
        for o in bpy.context.view_layer.objects.selected:
            o.select_set(False)
        context.view_layer.objects.active = obj
        context.active_object.select_set(True)

        bpy.context.scene.cursor.location = cursor_loc
        return {'FINISHED'}

class OBJECT_OT_assemble_collider_data(bpy.types.Operator):
    bl_idname = "op.assemble_collider_data"
    bl_label = "Assemble Collider Data from Children"
    bl_description = "Assemble Collider Data from Child Colliders"
    bl_options = {'REGISTER', 'UNDO'}# seems to work...

    def execute(self, context):
        # TODO !!!! .....there may be a bug, if data is already on one of them........ !!!!
        # but I can't reproduce right now

#        if len(context.selected_objects) != 1:
#            ShowMessageBox(message = "Please select one Object with one or more Child Colliders!")
#            return{'CANCELLED'}

        sel_objs = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
        bpy.ops.object.select_all(action='DESELECT')

        while len(sel_objs) >= 1:
            col_count = 0
#            obj = context.view_layer.objects.active
#            print(obj.name)
            obj = sel_objs.pop()
            col_data = []
            for child in obj.children:
                if child.get('CoolliderType') is None: # keine Ahnung, ob das geht oder active_object
                    continue
                col_count += 1
                col_data.append(child["CoolliderType"])
                for coord in child.location:
                    col_data.append(coord)
                for eul in child.rotation_euler:
                    col_data.append(eul)
                for dim in child.dimensions:
                    col_data.append(dim)
                
            if col_count > 0:
                obj["CoolliderCount"] = col_count
                obj["CoolliderData"] = col_data
            else: 
                if obj.get('CoolliderCount') is not None:
                    del obj["CoolliderCount"]
                if obj.get('CoolliderData') is not None:
                    del obj["CoolliderData"]

        return {'FINISHED'}


#--------------Helper------------------

def select_and_make_active(obj, deselect, context):
    if deselect:
        bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(True)
    context.view_layer.objects.active = obj

def ComputeCollider(context, inst, parent):
    if inst.col_type == 'Mesh':
        # duplicate mesh
        bpy.ops.object.duplicate(linked=False, mode='DUMMY')
        dup = context.view_layer.objects.active
        # apply all modifiers
        bpy.ops.object.convert(target='MESH', keep_original=False)
        # assign soft decimation modifier
        bpy.ops.object.modifier_add(type='DECIMATE')
        dup.modifiers["Decimate"].decimate_type = 'COLLAPSE'
        dup.modifiers["Decimate"].ratio = 0.3
    else:
        bpy.ops.mesh.primitive_cube_add()

    col = context.view_layer.objects.active
    col.display_type = 'BOUNDS'

    if inst.col_type == 'Box':
        col["CoolliderType"] = 0
        col.display_bounds_type = 'BOX'
        col.name = "BoxCollider"
    elif inst.col_type == 'Sphere':
        col["CoolliderType"] = 1
        col.display_bounds_type = 'SPHERE'
        col.name = "SphereCollider"
    elif inst.col_type == 'Capsule':
        col["CoolliderType"] = 2
        col.name = "CapsuleCollider"
        col.display_bounds_type = 'CAPSULE'
    elif inst.col_type == 'Mesh':
        col["CoolliderType"] = 3
        col.display_type = 'WIRE'
        col.name = "MeshCollider"

    col.data.name = col.name

    if inst.col_type == 'Capsule':
        pdim = parent.dimensions
        longest_on = 0
        longest_val = 0.0
        for i in range(3):
            if pdim[i] > longest_val:
                longest_val = parent.dimensions[i]
                longest_on = i

        if longest_on == 0:
            if pdim.y > pdim.z:
                col.dimensions = [pdim.y, pdim.y, pdim.x]
            else:
                col.dimensions = [pdim.z, pdim.z, pdim.x]
            # rotate 90 on y
            col.rotation_euler = Euler((0, radians(90), 0), 'XYZ')
        elif longest_on == 1:
            if pdim.x > pdim.z:
                col.dimensions = [pdim.x, pdim.x, pdim.y]
            else:
                col.dimensions = [pdim.z, pdim.z, pdim.y]
            # rotate 90 on x
            col.rotation_euler = Euler((radians(90), 0, 0), 'XYZ')
        else:
            if pdim.x > pdim.y:
                col.dimensions = [pdim.x, pdim.x, pdim.z]
            else:
                col.dimensions = [pdim.y, pdim.y, pdim.z]
            # (don't rotate)
    else:
        col.dimensions = parent.dimensions

    bpy.ops.object.transform_apply(location = False, scale = True, rotation = False)
    #select parent
    bpy.ops.object.select_all(action='DESELECT')
    parent.select_set(True)
    bpy.ops.object.duplicate(linked=False, mode='DUMMY')
    pardup = bpy.context.selected_objects[0]
    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
    bpy.ops.object.select_all(action='DESELECT')
    if inst.col_type != 'Mesh':
        col.parent = pardup

    col.select_set(True)
    parent.select_set(True)
    bpy.context.view_layer.objects.active = parent
    bpy.ops.object.parent_set(type='OBJECT', keep_transform=True)
    bpy.ops.object.select_all(action='DESELECT')
    pardup.select_set(True)
    bpy.ops.object.delete()
    return col

#----------------UI--------------------

def ShowMessageBox(message = "", title = "Message", icon = 'ERROR'):
    
    def draw(self, context):
        self.layout.label(text = message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

class SubMenu_Add(bpy.types.Menu):
    bl_idname = 'SubMenu_cool_add_menu'
    bl_label = 'Coollider Add'
    
    def draw(self, context):
        layout = self.layout
        layout.operator_enum(OBJECT_OT_add_collider.bl_idname, property = "col_type")

class SubMenu_Make(bpy.types.Menu):
    bl_idname = 'SubMenu_cool_make_menu'
    bl_label = 'Coollider Make'
    
    def draw(self, context):
        layout = self.layout
        layout.operator_enum(OBJECT_OT_make_collider.bl_idname, property = "col_type")

class OBJECT_MT_coollider_menu(bpy.types.Menu):
    bl_idname = 'OBJECT_MT_cool_menu'
    bl_label = 'Coollider'

    def draw(self, context):
        layout = self.layout
#        layout.operator_enum(OBJECT_OT_add_collider.bl_idname, property = "col_type")
#        layout.operator_enum(OBJECT_OT_make_collider.bl_idname, property = "col_type")
        layout.menu("SubMenu_cool_add_menu")
        layout.menu("SubMenu_cool_make_menu")
        #layout.operator(OBJECT_OT_mirror_collider.bl_idname)
        #layout.operator(OBJECT_OT_assemble_collider_data.bl_idname)
        layout.operator(OBJECT_OT_copy_active_to_selected.bl_idname)


def menu_func(self, context):
        self.layout.menu(OBJECT_MT_coollider_menu.bl_idname)

#-------------Register-----------------

classes = [OBJECT_OT_add_collider, 
        OBJECT_OT_make_collider, 
        OBJECT_OT_copy_active_to_selected, 
        OBJECT_OT_mirror_collider,
        OBJECT_OT_assemble_collider_data, 
        OBJECT_MT_coollider_menu, 
        SubMenu_Make,
        SubMenu_Add]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.VIEW3D_MT_object_context_menu.append(menu_func)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    bpy.types.VIEW3D_MT_object_context_menu.remove(menu_func)
